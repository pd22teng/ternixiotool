/*
 * semControl.c
 *
 *  Created on: May 21, 2015
 *      Author: teng
 */

#include "semControl.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO
                              (Linux-specific) */
};

int get_sem(const char *pathName, int id, int createFlag)
{
	int sem_id, sem_flag;

	if(createFlag != 0)
		sem_flag = 0666|IPC_CREAT;
	else
		sem_flag = 0666;

	sem_id = semget(ftok(pathName, id), 1, sem_flag); // the number of semaphore is 1

	return sem_id;
}

int init_sem(int sem_id, int init_val)
{
	union semun sem_union;

	sem_union.val = init_val;

	if(semctl(sem_id, 0, SETVAL, sem_union) == -1)
	{
		perror("Initialize semaphore");
		return -1;
	}
	return 0;
}

// delete the samephore from the system
int del_sem(int sem_id)
{
	union semun sem_union;
	if(semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
	{
		perror("Delete semaphore");
		return -1;
	}
	return 0;
}

int sem_p(int sem_id)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0; // The single semaphore value to be 0
	sem_b.sem_op = -1; //P operation
	sem_b.sem_flg = SEM_UNDO; // system will release the remained semaphore automatically

	if(semop(sem_id, &sem_b, 1) == -1) {
		perror("P operation failed");
		return -1;
	}
	return 0;
}

int sem_v(int sem_id)
{
	struct sembuf sem_b;
	sem_b.sem_num = 0; // The single semaphore value to be 0
	sem_b.sem_op = 1; // V operation
	sem_b.sem_flg = SEM_UNDO; // system will release the remained semaphore automatically

	if(semop(sem_id, &sem_b, 1) == -1) {
		perror("V operation failed");
		return -1;
	}
	return 0;
}


