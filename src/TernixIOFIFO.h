/*
 * TernixIOFIFO.h
 *
 *  Created on: May 19, 2015
 *      Author: teng
 */

#ifndef TERNIXIOFIFO_H_
#define TERNIXIOFIFO_H_

#include "TernixIOCmdDatatypes.h"

int initFIFO(void);
int fillCmdTxFIFO(const TernixIOCmdPacket * packet);
int writeFIFOCmd(void);
int readFIFOReplyMsg(void);
int closeFIFOsAndClearBuff(void);
#endif /* TERNIXIOFIFO_H_ */
