/*
 * TernixIOFIFO.c
 *
 *  Created on: May 19, 2015
 *      Author: teng
 */

#include "TernixIOFIFO.h"
#include "semControl.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <linux/limits.h>

#include <sys/time.h>
#include <sys/types.h>

#define TERNIXWR	"/tmp/ternixcmdfifo"
#define TERNIXRD	"/tmp/ternixreplyfifo"

#define CMD_SEM_NAME_ID	1
#define RPL_SEM_NAME_ID	2

int _cmd_tx_fifo_fd;
int _rpl_rx_fifo_fd;

char * _cmd_tx_buff_pt;
size_t _cmd_tx_buff_counter = 0;

char _cmd_tx_buff[PIPE_BUF];
char _rpl_rx_buff[PIPE_BUF];

int initFIFO(void)
{
	// Open CMD named pipe
	_cmd_tx_fifo_fd = open(TERNIXWR, O_WRONLY); // read opened first in Daemon

	if(_cmd_tx_fifo_fd == -1) {
		printf("Open named pipe: %s failed\n", TERNIXWR);
		return -1;
	}

	_rpl_rx_fifo_fd = open(TERNIXRD, O_RDONLY|O_NONBLOCK); // write will opened later as Daemon received CMD

	if(_rpl_rx_fifo_fd == -1)
	{
		printf("Open named pipe: %s failed\n", TERNIXRD);
		return -1;
	}

	return 0;
}

int fillCmdTxFIFO(const TernixIOCmdPacket * packet)
{
	int i, j;

	FILE * stream;
	stream = open_memstream(&_cmd_tx_buff_pt, &_cmd_tx_buff_counter);

	fprintf(stream, "%d", packet->header->val);
	for(i = 0; i < packet->count; i++)
		fprintf(stream, " %u:%c:%u", packet->pairs[i]->ioSelect, packet->pairs[i]->falg[0], packet->pairs[i]->ioValue);

	fprintf(stream, '\0');

	fclose(stream);
	strcpy(_cmd_tx_buff, _cmd_tx_buff_pt);
//	printf("CMD packet: %s\n", _cmd_tx_buff);

	return _cmd_tx_buff_counter;
}

int writeFIFOCmd(void)
{
	int nwrite = 0;

	if(_cmd_tx_buff_counter <= 0) {
		printf("No data to be sent, buff is empty!\n");
		return -1;
	}

	nwrite = write(_cmd_tx_fifo_fd, _cmd_tx_buff, PIPE_BUF);

	if(nwrite < 0) {
		printf("Write data to CMD pipe failed, ERRNO: %d\n", errno);
		return -1;
	}

	return 0;
}

int readFIFOReplyMsg(void)
{
	int nread = 0;
	memset(_rpl_rx_buff, 0, sizeof(_rpl_rx_buff));
	nread = read(_rpl_rx_fifo_fd, _rpl_rx_buff, PIPE_BUF);

	if(nread <= 0) {
		if(errno != EAGAIN)
			printf("Data read failed! Error: %d\n", errno);
		return -1;
	}

	printf("%s", _rpl_rx_buff);

	return nread;
}

int closeFIFOsAndClearBuff(void)
{
	if(_cmd_tx_buff_pt != NULL) {
		free(_cmd_tx_buff_pt);
		_cmd_tx_buff_pt = NULL;
		_cmd_tx_buff_counter = 0;
	}

	if(_cmd_tx_fifo_fd != 0) {
		close(_cmd_tx_fifo_fd);
		_cmd_tx_fifo_fd = 0;
	}

	if(_rpl_rx_fifo_fd != 0) {
		close(_rpl_rx_fifo_fd);
		_rpl_rx_fifo_fd = 0;
	}

	return 0;
}
