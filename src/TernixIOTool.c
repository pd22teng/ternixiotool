/*
 * Copyright (C) Prodevice Oy Copyright.
 *
 * Author: Teng Wu
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the PG_ORGANIZATION nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY	THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS-IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include "TernixIOCmdFunctions.h"
#include "TernixIOFIFO.h"
#include "errno.h"

static void signal_handler(int sign_no)
{
	if(sign_no == SIGPIPE)
		printf("Pipe blocked\n");

	closeFIFOsAndClearBuff();
	deleteCmdPacket();
	exit(0);
}

int main(int argc, char* argv[])
{
	// Open the named pipes
	if(initFIFO() < 0) exit(1);

	// Parse parameters and collect user commands message
	if(parseUserCmdArguments(argc, argv) < 0) exit(1);

	signal(SIGINT, signal_handler);
	signal(SIGTSTP, signal_handler);
	signal(SIGQUIT, signal_handler);
	signal(SIGPIPE, signal_handler);

	// Write user commands message
	if(writeFIFOCmd() < 0) exit(1);
	// Wait to read the reply msg from TernixIODaemon
	while(1) {
		if(readFIFOReplyMsg() > 0)
			break;
	}

	closeFIFOsAndClearBuff();
	deleteCmdPacket();
	exit(0);
}



