/*
 * TernixIOCmdFunctions.c
 *
 *  Created on: May 15, 2015
 *      Author: teng
 */

#include "TernixIOCmdFunctions.h"
#include "TernixIOFIFO.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/poll.h>

TernixIOCmdPacket _cmd_packet;

static bool isAllDigit(const char * str);
static bool isPipeCommandDetected(int8_t * pdt);

int8_t parseUserCmdArguments(int argc, char *argv[])
{
	initCmdPacket();

	if(getIOCmdHeader(argv[0]) < 0) {
		printf("The CMD: %s is not supported!\n", argv[0]);
		print_usage();
		return -1;
	}

	if(argc > 1) {
		if(getIOCmdPairs(argc, argv) < 0)
		{
			printf("Parsing user CMD failed!\n");
			return -1;
		}
	}

	if(fillCmdTxFIFO(&_cmd_packet) <= 0)
	{
		printf("Data collect failed!\n");
		return -1;
	}

	deleteCmdPacket();

	return 0;
}

int8_t getIOCmdHeader(const char* cmdName)
{
	int i;

	for(i = 0; i < TERNIX_IO_CMD_COUNT; i++) {
		if(strcmp(ternixIOCmdTable.items[i]->cmdName, cmdName) == 0) {
			_cmd_packet.header = malloc(sizeof(TernixIOCmdHeader));
			memcpy(_cmd_packet.header, ternixIOCmdTable.items[i]->header, sizeof(TernixIOCmdHeader));
			return 0;
		}
	}

	return -1;
}

int8_t getIOCmdPairs(int argc, char *argv[])
{
	int8_t cmd_id = _cmd_packet.header->bits.id;

	const char *short_opt = ternixIOCmdTable.items[cmd_id]->options->shortOptStr;
	const struct option *long_opt = ternixIOCmdTable.items[cmd_id]->options->longopt;

	switch(cmd_id) {
		case TERNIX_READ_INPUT:
		case TERNIX_READ_OUTPUT:
		case TERNIX_WRITE_OUTPUT:
			// parsing options and make CMD message to send
			while(getUserCmdOption(argc, argv, short_opt, long_opt, NULL) != -1);
			// if not supported args
			if(optind < argc) {
				printf("non-option ARGV-elements <%s>: ", argv[0]);
				while(optind < argc)
					printf("%s ", argv[optind++]);
				putchar('\n');
				print_usage();
				return -1;
			}
			break;
		default:
			printf("CMD error\n");
			print_usage();
			return -1;
	}
	return 0;
}

static bool isAllDigit(const char * str)
{
	int i = 0;

	if(str == NULL)
		return false;

	while(str[i] != '\0' && str[i] != '\n') {
		if(isdigit(str[i++]) == 0) {
			return false;
		}
	}

	return true;
}

int8_t getUserCmdOption(int argc, char *const *argv, const char *shortopts, const struct option *longopts, int *longind)
{
	int opt;
	TernixIOCmdMessagePair pair;
	int8_t count = 0;
	char *opt_val[32];
	char tmp[64];
	char *pch;
	int8_t begin = 0, end = 0, bit = 0, i;
	uint32_t dec_val = 0, dec_sel = 0;

	opt = getopt_long(argc, argv, shortopts, longopts, longind);

	if(opt < 0)
		return -1;

	getOptionParameters(argc, argv, opt_val, &count);

	if(count <= 0) {
		printf("No arguments found for option '%c'\n", (char)opt);
		return -1;
	}

	initTernixIOCmdMessagePair(&pair, 0, opt, 0);

	switch(opt)
	{
		case 'f':
			if(strcmp("dec", opt_val[0]) == 0)
				_cmd_packet.header->bits.cmdFormat = TERNIX_DEC_FORMAT;
			else {
				printf("Error: invalid parameter for option '%c'\n", (char)opt);
				return -1;
			}

			if(count == 1) { // pipe info
				if(_cmd_packet.header->bits.id == TERNIX_WRITE_OUTPUT) {
					memset(tmp, '\0', 64);
					if(isPipeCommandDetected(tmp)) {
						opt_val[1] = tmp;
						count = 2;
					}
					else
					{
						printf("Error: Parameter missing for option '%c'\n", (char)opt);
						return 01;
					}
				}
			}

			if(count == 2) {
				if(isAllDigit(opt_val[1])) {
					dec_val = (uint32_t)atoll(opt_val[1]);
					if((_cmd_packet.pairs[_cmd_packet.count++] = createTernixIOCmdMessagePair(0, opt, dec_val)) == NULL)
						return -1;
				} else {
					printf("Error: invalid parameter: '%s' for option '%c'\n", opt_val[1], (char)opt);
					return -1;
				}
			}
			else if(count > 2) {
				printf("Error: invalid parameter for option '%c'\n", (char)opt);
				return -1;
			}
			break;
		case 'a':
		case 'd':
			// -a IN1 IN2 IN3 ...
			for(i = 0; i < count; i++) {
				if(strchr(opt_val[i], '-') != NULL) {
					pch = strtok(opt_val[i], "-"); // -a INx-INy
					begin = atoi(pch);
					pch = strtok(NULL, "-");
					end = atoi(pch);

					for(i = begin; i <= end; i++)
						pair.ioSelect |= (1 << (i - 1));
				} else {
					bit = atoi(opt_val[i]);
					if(bit == 0) {
						printf("Error: invalid parameter for option '%c'\n", (char)opt);
						return -1;
					}
					pair.ioSelect |= (1 << (bit - 1));
				}
			}
			if((_cmd_packet.pairs[_cmd_packet.count++] = createTernixIOCmdMessagePair(pair.ioSelect, pair.falg[0], pair.ioValue)) == NULL)
				return -1;
			break;
		case 'o':
			for(i = 0 ; i < count; i++) {
				if(strchr(opt_val[i], '=') != NULL) {
					pch = strtok(opt_val[i], "=");
					bit = atoi(pch);
					pch = strtok(NULL, "=");
					if(isAllDigit(pch))
					{
						dec_val = atoi(pch);
						if(dec_val > TERNIX_ANALOG_MAX_VALUE)
							dec_val = TERNIX_ANALOG_MAX_VALUE;

						if((_cmd_packet.pairs[_cmd_packet.count++] = createTernixIOCmdMessagePair(bit, 'a', dec_val)) == NULL)
							return -1;
					}
					else if(strcmp("OFF", pch) == 0) {
						pair.ioSelect |= (1 << (bit - 1));
						pair.ioValue &= (~(1 << (bit - 1)));
					}
					else if(strcmp("ON", pch) == 0) {
						pair.ioSelect |= (1 << (bit - 1));
						pair.ioValue |= (1 << (bit - 1));
					}
					else {
						printf("Error: invalid parameter for option '%c'\n", (char)opt);
						return -1;
					}
				} else {
					bit = atoi(opt_val[i]);
					if(bit == 0) {
						printf("Error: invalid parameter for option '%c'\n", (char)opt);
						return -1;
					} else
						pair.ioSelect |= (1 << (bit - 1));
				}
			}
			// digit output selected
			if(pair.ioSelect) {
				if((_cmd_packet.pairs[_cmd_packet.count++] = createTernixIOCmdMessagePair(pair.ioSelect, pair.falg[0], pair.ioValue)) == NULL)
					return -1;
			}
			break;
			default:
				printf("Error: invalid option '%c'\n", (char)opt);
				return -1;
				break;
	}
	return 0;
}

void getOptionParameters(int argc, char *const *argv, char **vals, int8_t *count)
{
	optind--;
	for(; optind < argc && *argv[optind] != '-'; optind++) {
		vals[*count] = argv[optind];
		(*count) += 1;
	}
}

bool isPipeCommandDetected(int8_t * pdt)
{
	if(fgets(pdt, 64, stdin) == NULL)
	{
		printf("Pipe input data read failed\n");
		return false;
	}

	return true;
}

void initCmdPacket(void)
{
	int8_t i = 0;

	_cmd_packet.header = NULL;
	_cmd_packet.count = 0;

	for (i = 0; i < 32; ++i) {
		_cmd_packet.pairs[i] = NULL;
	}
}

void deleteCmdPacket(void)
{
	int i;

	if(_cmd_packet.header != NULL) {
		free(_cmd_packet.header);
		_cmd_packet.header = NULL;
	}

	for(i = 0; i < _cmd_packet.count; i++) {
		if(_cmd_packet.pairs[i] != NULL) {
			free(_cmd_packet.pairs[i]);
			_cmd_packet.pairs[i] = NULL;
		}
	}
	_cmd_packet.count = 0;
}

void print_usage(void)
{
	fprintf(stderr, "Usage: Ternix-read-inputs [options]\n"
			"options:\n"
			"-d, --digit=INx\t" "digital input IDs(1-32)(eg. Ternix-read-inputs -d 1 2 4)\n"
			"-a, --analog=INx\t" "digital input IDs(1-32)(eg. Ternix-read-inputs -a 1 2 4)\n"
			"-f, --format=dec\t" "set read inputs reply format to dec(eg. Ternix-read-inputs -f dec -d 1-5)\n"
			"-h, --help\t\t" "this help\n"
			"--------------------------------------\n"
			"Usage: Ternix-read-outputs [options]\n"
			"options:\n"
			"-o, --output=OUTx\t" "read output IDs(1-32)(eg. Ternix-read-outputs -o 3 5 12)\n"
			"-h, --help\t\t" "this help\n"
			"--------------------------------------\n"
			"Usage: Ternix-write-outputs [options]\n"
			"options:\n"
			"-o, --output OUTx=HI OUTx=LOW OUTx=512\t" "write output IDs(1-32)(eg. Ternix-write-outputs -o 3=HI 5=LOW 12=347)\n"
			"-f, --format=dec\t" "set write outputs format to dec(eg. Ternix-write-outputs -f dec 348)\n"
			"-h, --help\t\t" "this help\n"
			);
}

