/*
 * TernixIOCmd.c
 *
 *  Created on: May 12, 2015
 *      Author: teng
 */

#include <stdio.h>
#include "TernixIOCmdDatatypes.h"

const char ternixReadInputCmdName[]   = "Ternix-read-inputs";
const char ternixReadOutputCmdName[]  = "Ternix-read-outputs";
const char ternixWriteOutputCmdName[] = "Ternix-write-outputs";

const char ternixReadInputCmdShortOptStr[]   = "f:a:d:h:";
const char ternixReadOutputCmdShortOptStr[]  = "o:h:";
const char ternixWriteOutputCmdShortOptStr[] = "f:o:h:";

const char helpOptLongName[] = "help";
const char formatOptLongName[] = "format";
const char analogOptLongName[] = "analog";
const char digitOptLongName[] = "digital";
const char outputOptLongName[] = "output";

const struct option help   = {helpOptLongName,    no_argument,       NULL, 'h'};
const struct option format = {formatOptLongName,  required_argument, NULL, 'f'};
const struct option analog = {analogOptLongName,  required_argument, NULL, 'a'};
const struct option digit  = {digitOptLongName,   required_argument, NULL, 'd'};
const struct option output = {outputOptLongName,  required_argument, NULL, 'o'};
const struct option nano   = {0, 0, 0, 0};


const struct option readInputLongOptions[] = {{helpOptLongName, no_argument, NULL, 'h'},
											  {formatOptLongName, required_argument, NULL, 'f'},
											  {analogOptLongName, required_argument, NULL, 'a'},
											  {digitOptLongName, required_argument, NULL, 'd'},
											  {0, 0, 0, 0}};

const struct option readOutputLongOptions[] = {{helpOptLongName, no_argument, NULL, 'h'},
											   {outputOptLongName, required_argument, NULL, 'o'},
											   {0, 0, 0, 0}};

const struct option writeOutputLongOptions[] = {{helpOptLongName, no_argument, NULL, 'h'},
												{outputOptLongName, required_argument, NULL, 'o'},
												{formatOptLongName, required_argument, NULL, 'f'},
												{0, 0, 0, 0}};

const TernixIOCmdOptions readInputCmdOptions = {
	ternixReadInputCmdShortOptStr,
	readInputLongOptions
};

const TernixIOCmdOptions readOutputCmdOptions = {
	ternixReadOutputCmdShortOptStr,
	readOutputLongOptions
};

const TernixIOCmdOptions writeOutputCmdOptions = {
	ternixWriteOutputCmdShortOptStr,
	writeOutputLongOptions
};

const TernixIOCmdHeader readInputCmdHeader = {	.bits.id = TERNIX_READ_INPUT, .bits.io = TERNIX_INTPUT,
												.bits.ioAction = TERNIX_READ,
												.bits.cmdFormat = TERNIX_TEXT_FORMAT };

const TernixIOCmdHeader readOutputCmdHeader = {  .bits.id = TERNIX_READ_OUTPUT, .bits.io = TERNIX_OUTPUT,
										 	 	 .bits.ioAction = TERNIX_READ,
										 	 	 .bits.cmdFormat = TERNIX_TEXT_FORMAT};

TernixIOCmdHeader writeOutputCmdHeader = {  .bits.id = TERNIX_WRITE_OUTPUT, .bits.io = TERNIX_OUTPUT,
									      	.bits.ioAction = TERNIX_WRITE,
									      	.bits.cmdFormat = TERNIX_TEXT_FORMAT};

const TernixIOCmdItem ternixReadInputs = {
	ternixReadInputCmdName,
	&readInputCmdHeader,
	&readInputCmdOptions
};

const TernixIOCmdItem ternixReadOutputs = {
	ternixReadOutputCmdName,
	&readOutputCmdHeader,
	&readOutputCmdOptions
};

const TernixIOCmdItem ternixWriteOutputs = {
	ternixWriteOutputCmdName,
	&writeOutputCmdHeader,
	&writeOutputCmdOptions
};

const TernixIOCmdTable ternixIOCmdTable = {
	TERNIX_IO_CMD_COUNT,
	{&ternixReadInputs, &ternixReadOutputs, &ternixWriteOutputs}
};

TernixIOCmdMessagePair* createTernixIOCmdMessagePair(uint32_t ioSelect, char flag, uint32_t ioValue)
{
	TernixIOCmdMessagePair* pair = NULL;

	pair = malloc(sizeof(TernixIOCmdMessagePair));

	if(pair == NULL) {
		printf("Cannot create new CMD Msg pair!\n");
		return NULL;
	}

	initTernixIOCmdMessagePair(pair, ioSelect, flag, ioValue);

	return pair;
}

int initTernixIOCmdMessagePair(TernixIOCmdMessagePair* pair, uint32_t ioSelect, char flag, uint32_t ioValue)
{
	if(pair == NULL)
		return -1;

	pair->ioSelect = ioSelect;
	pair->falg[0] = flag;
	pair->ioValue = ioValue;

	return 0;
}
