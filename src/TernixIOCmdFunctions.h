/*
 * TernixIOCmdFunctions.h
 *
 *  Created on: May 15, 2015
 *      Author: teng
 */

#ifndef TERNIXIOCMDFUNCTIONS_H_
#define TERNIXIOCMDFUNCTIONS_H_

#include "TernixIOCmdDatatypes.h"

void initCmdPacket(void);
void deleteCmdPacket(void);

int8_t parseUserCmdArguments(int argc, char *argv[]);
int8_t getIOCmdHeader(const char* cmdName);
int8_t getIOCmdPairs(int argc, char *argv[]);
int8_t getUserCmdOption(int argc, char *const *argv, const char *shortopts, const struct option *longopts, int *longind);
void getOptionParameters(int argc, char *const *argv, char **vals, int8_t *count);
void print_usage(void);

#endif /* TERNIXIOCMDFUNCTIONS_H_ */
